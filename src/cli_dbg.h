#ifndef CLI_DBG_H
#define CLI_DBG_H

enum CliDbgType {
	CLI_DBG_NONE,
	CLI_DBG_ERROR,
	CLI_DBG_FUNC,
	CLI_DBG_INFO,
	CLI_DBG_MSG,
	CLI_DBG_MAX,
};

void cli_dbg_print(CliDbgType type, const char *format, ...);
void cli_log_print(int level, const char *format, ...);

#define CMD_debug(x, format, ...) cli_dbg_print(x, format, ##__VA_ARGS__)

#define CMD_DBGASSERT(x, format, ...) \
if (0 == x) {\
	printf("\r\nAssert at %s:%d. ", __FILE__, __LINE__);\
	printf(format, ##__VA_ARGS__);\
	cli_log_print(0, "Assert at %s:%d.", __FILE__, __LINE__);\
	cli_log_print(0, format, ##__VA_ARGS__);\
}

#endif