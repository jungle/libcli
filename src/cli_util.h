#ifndef __CLI_UTIL_H__
#define __CLI_UTIL_H__
#include <string>

using namespace std;

int cmd_util_time_to_string(string &time_string, const time_t &time_data);
BOOL cmd_string_isdigit(CHAR *string);
ULONG cmd_ip_string_to_ulong(CHAR *ip);
VOID cmd_ip_ulong_to_string(ULONG ip, CHAR *buf);
BOOL cmd_string_is_ip(CHAR *str);
CHAR *cmd_util_strdup(const char *s);
int cmd_util_strnicmp(const char *dst, const char *src, int count);
int cmd_util_stricmp(const char *dst, const char *src);

#endif