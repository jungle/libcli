#include "cli_dbg.h"
#include "cli_ctx.h"
#include "stdio.h"
#include <stdarg.h>

void cli_dbg_print(CliDbgType type, const char *format, ...)
{
    if (!g_cli_ctx.dbgPrint) {
		return;
	}

    char buf[512];
    va_list ap;
 
    va_start(ap, format);
    vsnprintf(buf, sizeof(buf), format, ap);
    va_end(ap);
    
    g_cli_ctx.dbgPrint(g_cli_ctx.userInfo, type, "%s", buf);
    return;
}

void cli_log_print(int level, const char *format, ...)
{
    if (!g_cli_ctx.logPrint) {
		return;
	}

    char buf[512];
    va_list ap;
 
    va_start(ap, format);
    vsnprintf(buf, sizeof(buf), format, ap);
    va_end(ap);
    
    g_cli_ctx.logPrint(level, "%s", buf);
    return;
}