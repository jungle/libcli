#ifndef __CLI_CTX_H__
#define __CLI_CTX_H__

#include "icli.h"

extern CliCtx g_cli_ctx;

char *cmd_get_sysname();
void cmd_page_up();
void cmd_page_down();
int cmd_socket_send(int s, char *buf, int len, int flags);
int cmd_socket_recv(int s, char *buf, int len, int flags);
int cmd_socket_close(int s);
int cmd_sleep(int ms);
int cmd_getchar(void);
bool cmd_is_cfg_recover_over();

#endif